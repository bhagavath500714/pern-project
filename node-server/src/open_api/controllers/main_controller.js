const pool = require("../../db/db");

//Create file
module.exports.createInfo = async (req, res) => {
    try {
        // console.log(req.body);
        // const { description } = req.body;
        const newTodo = await pool.query(
            "INSERT INTO school (NAME, AGE, ADDRESS, EMAIL, DATEOFJOINING, YEAROFEXPERIENCEATJOINING, TECHNOLOGIESKNOWN, TECHNOLOGIESTOLEARN) VALUES($1,$2,$3,$4,$5,$6,$7,$8) RETURNING *",[req.body.name,req.body.age,req.body.address,req.body.email,req.body.dateofjoining,req.body.yearofexperienceatjoining,req.body.technologiesknown,req.body.technologiestolearn]
        );
        return res.json(newTodo.rows[0]);
    } catch (err) {
        console.error(err.message);
        return next(err);
    }
}

// Get all 
module.exports.getAllInfo = async (req, res) => {
    try {
        const school = await pool.query("SELECT * from school");
        return res.json(school.rows);
    } catch (err) {
        console.error(err.message)
        return next(err);
    }
}

// Get by Id
module.exports.getInfoById = async (req, res) => {
    try {
        //  console.log(req.params)  
        const { id } = req.params;
        const todo = await pool.query("SELECT * FROM school WHERE school_id = $1", [id]);
    
        return res.json(todo.rows[0]);
        
        } catch (err) {
           console.error(err.message) 
           return next(err);
        }
}

// update
module.exports.updateInfo = async (req, res) => {
    try {
        //  console.log(req.params)  
        // const { id } = req.params;
        // const { description } = req.body;
        const updateTodo = await pool.query("UPDATE school SET name=$1, age=$2, address=$3, email=$4, dateofjoining=$5, yearofexperienceatjoining=$6, technologiesknown=$7, technologiestolearn=$8 WHERE school_id=$9 RETURNING *", [req.body.name,req.body.age,req.body.address,req.body.email,req.body.dateofjoining,req.body.yearofexperienceatjoining,req.body.technologiesknown,req.body.technologiestolearn, req.params.id]);
    
        res.json("Todo was updated!");
        
        } catch (err) {
           console.error(err.message)
           return next(err); 
        }
}

// Delete

module.exports.deleteInfo = async (req, res) => {
    try {
        //  console.log(req.params)  
        const { id } = req.params;
        const deleteTodo = await pool.query("DELETE FROM school WHERE school_id = $1", [id]);
    
        return res.json("Todo was deleted!");
        
        } catch (err) {
           console.error(err.message) 
           return next(err);
        }
}
