var controllerImdb = require('../controllers/main_controller');

module.exports = function (router) {
    router
        .route('/api/add')
        .post(controllerImdb.createInfo);
    router
        .route('/api/info/all')
        .get(controllerImdb.getAllInfo);

    router
        .route('/api/infoById/:id')
        .get(controllerImdb.getInfoById);
    
    router
        .route('/api/updateInfo/:id')
        .put(controllerImdb.updateInfo);
    router
        .route('/api/delete/:id')
        .delete(controllerImdb.deleteInfo);
    return router;
}
