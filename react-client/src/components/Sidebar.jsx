import React from 'react'
import { Link, useHistory } from 'react-router-dom'

export default function Sidebar() {
    return (
        <div className="appSidebar">
                    <ul style={{display: 'flex', flexDirection: 'column'}}>
                    <Link className="button_base btn_radius button_bordered" to="/about">Dashboard </Link>
                    <Link className="button_base btn_radius button_bordered" to="/about">About </Link>
                    <Link className="button_base btn_radius button_bordered" to="/services">Services </Link>
            </ul>
        </div>
    )
}
