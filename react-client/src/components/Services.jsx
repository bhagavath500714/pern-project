import React from 'react'
import Header from './Header';
import Sidebar from './Sidebar';

export default function Services() {
    return (
        <>
            <Header/>
            <div style={{display: 'flex'}}>
                <Sidebar />
                <div className="appMain">
                    <h1>Services</h1>
                </div>
            </div>
        </>
    )
}
