import React, { useState } from 'react'

const LoginForm = ({Login, error }) => {
    const [details, setDetails] = useState({name: '', email: '', password: ''});


    const submitHandler = e => {
        e.preventDefault();

        Login(details);
    }

    return (
        <div className="loginContainer">
            <form onSubmit={submitHandler}>
                <div className="loginWrapper">
                    <h2>Login</h2>
                    {(error !== "") ? (<div className="formError">{error}</div>) : ''}
                    <div className="formGroup">
                        <label htmlFor="name" className="formLabel">Name:</label>
                        <input className="formInput" type="text" name="name" id="name" onChange={e => setDetails({...details, name: e.target.value})} value={details.name} />
                    </div>
                    <div className="formGroup">
                        <label htmlFor="email" className="formLabel">Email:</label>
                        <input className="formInput" type="text" name="email" id="email" onChange={e => setDetails({...details, email: e.target.value})} value={details.email} />
                    </div>
                    <div className="formGroup">
                        <label htmlFor="password" className="formLabel">Password:</label>
                        <input className="formInput" type="text" name="password" id="password" onChange={e => setDetails({...details, password: e.target.value})} value={details.password} />
                    </div>
                    <div className="actionGroup">
                        <input type="submit" value="LOGIN" className="formBtn"/>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default LoginForm