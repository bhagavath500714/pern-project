import React from 'react'
import Header from './Header';
import Sidebar from './Sidebar';

export default function About (props) {
    // const {name} = props.user;
    // console.log(name);
    return (
        <>
            <Header/>
            <div style={{display: 'flex'}}>
                <Sidebar />
                <div className="appMain">
                    <h1>About</h1>
                </div>
            </div>
        </>
    )
}
