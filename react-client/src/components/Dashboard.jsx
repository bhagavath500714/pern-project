// import React from 'react'
// import {
//     useParams
//   } from 'react-router-dom'

// export default function Dashboard(props) {
//     const {username} = useParams();
//     console.log(username);
//     return (
//         <div>
//            <h1>Dashboard {username}</h1> 
//         </div>
//     )
// }

import React from 'react'
import { Switch,Route, useRouteMatch } from 'react-router-dom'
import Header from './Header';
import Sidebar from './Sidebar';
import About from './About';

export default function Dashboard(props) {
    const {name} = props.user;
    const { path } = useRouteMatch()
    console.log(name);
    return (
        <>
            <Header name={name}/>
            <div style={{display: 'flex'}}>
                <Sidebar />
                <div className="appMain">
                    <h1>Dashboard</h1>
                    <h3>Welcome {name}</h3>
                </div>
            </div>
        </>
    )
}
