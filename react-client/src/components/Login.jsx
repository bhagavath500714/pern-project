// import React, {useState} from 'react'
// import { useHistory } from 'react-router-dom'




// export default function Login(props) {
//     const [error, setError] = useState('');
//     const history = useHistory()

//     let format = {
//         color: "red"
//     };

//     const handleSubmit = (e) => {
//         e.preventDefault();
//         var username = e.target.elements.username.value;
//         var password = e.target.elements.password.value;

//         if(username == 'abc' && password == '123') {
//             history.push('/dashboard/' + username);
//         } else {
//             setError('Invalid');
//         }
//     }
//     return (
//         <div>
//             <span style={format}>{error !== '' ? error : ''}</span>
//             <h3>Login</h3>
//             <form method="post" onSubmit={handleSubmit}>
//                 username <input type="text" name="username" />
//                 password <input type="password" name="password" />
//                 <input type="submit" value="Login" />
//             </form>
//         </div>
//     )
// }

import React, { useState } from 'react'
import LoginForm from '../components/LoginForm'
import Dashboard from './Dashboard';

function Login () {
    const adminUser = {
        email: "admin@admin.com",
        password: "admin123"
    }

    const [user, setUser] = useState({name: "", email: ""});
    const [error, setError] = useState("");

    const handleLogin = details => {
        console.log(details);

        if (details.email == adminUser.email && details.password == adminUser.password) {
            console.log('Logged In');
            setUser({
                name: details.name,
                email: details.email
            });
        } else {
            console.log('Details do not match');
            setError('Details do not match');
        }
    }

    const Logout = () => {
        console.log("Logout");
        setUser({ name: "", email: "" });
    }

    return (
        <div className="App">
            {(user.email !== "") ? (
                <Dashboard user={user}/>
            ) : (
                <LoginForm Login={handleLogin} error={error} />
            )}
        </div>
    );
}

export default Login