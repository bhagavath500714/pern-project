import React, {createContext} from 'react'
import { BrowserRouter as Router,Switch,Route } from 'react-router-dom'

import Login from './components/Login'
import Dashboard from './components/Dashboard'
import About from './components/About';
import Services from './components/Services';


function Routes(props) {
  console.log('routes', props)
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Login />
        </Route>
        <Route exact path="/dashboard">
          <Dashboard />
          </Route>
          <Route exact path="/about">
          <About />
          </Route>
          <Route exact path="/Services">
          <Services />
          </Route>
      </Switch>
    </Router>
  )
}

export default Routes